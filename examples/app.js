const express = require('express')
const bodyParser = require('body-parser')

const app = express()

//recibe datos json del navegador
app.use(bodyParser.json({}))
//este lee los datos directamente de postman como formulario
app.use(bodyParser.urlencoded({extended: false}))

//the name of this is mocking
const places = [
    {
        'title':'Raven Egg Development',
        'description': 'Lorem Ipsum',
        'address': 'Lorem Ipsum'
    },
    {
        'title':'Raven Egg Development',
        'description': 'Lorem Ipsum',
        'address': 'Lorem Ipsum'
    },
    {
        'title':'Raven Egg Development',
        'description': 'Lorem Ipsum',
        'address': 'Lorem Ipsum'
    },
    {
        'title':'Raven Egg Development',
        'description': 'Lorem Ipsum',
        'address': 'Lorem Ipsum'
    }
]

app.get('/', (req,res)=>{
    res.json(places)
})

app.post('/', (req, res)=>{
    res.json(req.body.name)
})

//todo aquello que este dentro de esta carpeta esta disponible
app.use(express.static('public'))

//3000, 8080, 8000
//localhost es un sobrenombre 127.0.0.1
app.listen(3000, function(){
    console.log("ready for requests")
})